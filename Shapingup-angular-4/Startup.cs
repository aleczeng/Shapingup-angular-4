﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Shapingup_angular_4.Startup))]
namespace Shapingup_angular_4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
